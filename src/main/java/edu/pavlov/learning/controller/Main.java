package edu.pavlov.learning.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = {"/","/api"})
@RestController
public class Main {

    private static int count = 0;

    @GetMapping
    public @ResponseBody String getRootMessage(){
        return "Hello World. This page was visited " + count++ + " times. api: v4 (database config logs)";
    }
}
