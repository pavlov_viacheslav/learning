package edu.pavlov.learning.controller;

import edu.pavlov.learning.model.User;
import edu.pavlov.learning.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/{userName}/save", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User saveUser(@PathVariable String userName) {
        User newUser = new User();
        newUser.setName(userName);

        return userService.saveUser(newUser);
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<User> list() {
        return userService.getAllUsers();
    }
}
