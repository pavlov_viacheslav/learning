package edu.pavlov.learning.service.impl;

import edu.pavlov.learning.model.User;
import edu.pavlov.learning.repository.UserRepository;
import edu.pavlov.learning.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public User saveUser(User input) {
        return userRepository.save(input);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
