package edu.pavlov.learning.service;

import edu.pavlov.learning.model.User;

import java.util.List;

public interface UserService {

    User saveUser(User input);

    List<User> getAllUsers();
}
