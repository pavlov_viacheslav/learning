package edu.pavlov.learning.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


import java.util.ArrayList;
import java.util.List;

@Configuration
@Import({WebConfig.class, SecurityConfig.class, DatabaseConfig.class})
public class AppConfig {

    @Bean
    public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
        final PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setIgnoreResourceNotFound(false);
        propertyPlaceholderConfigurer.setSystemPropertiesMode(PropertyPlaceholderConfigurer.SYSTEM_PROPERTIES_MODE_OVERRIDE);
        propertyPlaceholderConfigurer.setFileEncoding("utf-8");

        final List<Resource> locations = new ArrayList<>();
        locations.add(new ClassPathResource("application.properties"));

        propertyPlaceholderConfigurer.setLocations(locations.toArray(new Resource[]{}));
        return propertyPlaceholderConfigurer;
    }


}

