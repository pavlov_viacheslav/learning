package edu.pavlov.learning.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"edu.pavlov.learning.controller"})
public class WebConfig extends WebMvcConfigurerAdapter {

    @Value("${base.url}")
    private String baseUrl;

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".html");

        return viewResolver;
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }


    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        registry.addMapping("/api/**").allowedMethods("PUT", "POST", "GET", "DELETE");
//        registry.addMapping("/html/**").allowCredentials(false).allowedMethods("GET", "OPTIONS", "HEAD").allowedOrigins(baseUrl).maxAge(3600);
    }
}

