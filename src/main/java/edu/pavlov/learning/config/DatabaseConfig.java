package edu.pavlov.learning.config;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = { "edu.pavlov.learning" }, excludeFilters = { @ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
@ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION) }, includeFilters = @ComponentScan.Filter(value = Entity.class, type = FilterType.ANNOTATION))
@EnableJpaRepositories(basePackages = "edu.pavlov.learning.repository", entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager", repositoryImplementationPostfix = "CustomImpl")
@EnableTransactionManagement
public class DatabaseConfig {

    private static Logger logger = LogManager.getLogger(DatabaseConfig.class);

    private final static String packagesToScan = "edu.pavlov.learning.service.impl";

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.driverClassName}")
    private String dbDriverClassName;

    @Value("${db.username}")
    private String dbUsername;

    @Value("${db.password}")
    private String dbPassword;

    @Value("${db.showSql}")
    private boolean dbShowSql;

    @Value("${db.hbm2ddl}")
    private boolean dbGenerateDdl;

    @Value("${db.hbm2ddl.auto}")
    private String dbGenerateDdlAuto;

    @Value("${hibernate.dialect}")
    private String hibernateDialect;

    @Bean
    public DataSource getDataSource() {
        logger.info("DataSource initialization started, {}, {}, {}, {}", dbDriverClassName, dbUrl, dbUsername, dbPassword);
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(dbDriverClassName);
        ds.setUrl(dbUrl);
        ds.setUsername(dbUsername);
        ds.setPassword(dbPassword);
        return ds;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        logger.info("SessionFactory initialization started");
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(getDataSource());
        sessionFactory.setHibernateProperties(hibernateProperties());
        sessionFactory.setPackagesToScan(packagesToScan);

        return sessionFactory;
    }

    @Bean
    public JPAQueryFactory jpaQueryFactory(EntityManager entityManager) {
        return new JPAQueryFactory(entityManager);
    }

    @Bean
    public JpaTransactionManager transactionManager(final DataSource dataSource, final EntityManagerFactory entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setDataSource(dataSource);
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.connection.driver_class", dbDriverClassName);
        hibernateProperties.setProperty("hibernate.dialect", hibernateDialect);
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", dbGenerateDdlAuto);

        return hibernateProperties;
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
        final LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
        entityManager.setDataSource(getDataSource());
        entityManager.setPersistenceUnitName("spring-jpa");
        entityManager.setJpaVendorAdapter(jpaVendorAdapter());
        entityManager.setPersistenceProvider(new HibernatePersistenceProvider());
        final Properties jpaProperties = jpaProperties();
        for (final Object key : jpaProperties.keySet()) {
            System.err.println("JPA: " + key + " = " + jpaProperties.get(key));
        }

        entityManager.setJpaProperties(jpaProperties);
        entityManager.setPackagesToScan("edu.pavlov.learning.model");

        return entityManager;
    }

    private Properties jpaProperties() throws Exception {
        final Properties jpaProp = new Properties();
        jpaProp.put("hibernate.dialect", hibernateDialect);
        return jpaProp;
    }


    private HibernateJpaVendorAdapter jpaVendorAdapter() {
        final HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(dbShowSql);
        jpaVendorAdapter.setGenerateDdl(dbGenerateDdl);
        return jpaVendorAdapter;
    }
}
