FROM java:8
FROM jetty:9.4-jre8
USER root

ADD *war /var/lib/jetty/webapps/root.war
WORKDIR /var/lib/jetty

EXPOSE 8080